package personal.toastsimple;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button buttonIn;
    private EditText editTextIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initComponents();
    }


    private void initComponents() {
        this.buttonIn = (Button) this.findViewById(R.id.button_in);
        this.editTextIn = (EditText) this.findViewById(R.id.edit_text_in);

        this.buttonIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = MainActivity.this.editTextIn.getText().toString();
                Toast toast = Toast.makeText(MainActivity.this.getApplicationContext(), text, Toast.LENGTH_LONG);
                toast.show();
            }
        });
    }
}
